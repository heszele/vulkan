#include <iostream>
#include <vector>
#include <set>
#include <optional>
#include <functional>
#include <algorithm>
#include <exception>

#define NOMINMAX
#include <Windows.h>
//#include <libloaderapi.h>

#define VK_USE_PLATFORM_WIN32_KHR
#include <vulkan/vulkan.h>
// #define VULKAN_HPP_NO_EXCEPTIONS
#include <vulkan/vulkan.hpp>
#include <vulkan/vulkan_raii.hpp>

#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Copied from: https://github.com/KhronosGroup/glslang/blob/master/StandAlone/ResourceLimits.cpp
const TBuiltInResource DefaultTBuiltInResource = {
    /* .MaxLights = */ 32,
    /* .MaxClipPlanes = */ 6,
    /* .MaxTextureUnits = */ 32,
    /* .MaxTextureCoords = */ 32,
    /* .MaxVertexAttribs = */ 64,
    /* .MaxVertexUniformComponents = */ 4096,
    /* .MaxVaryingFloats = */ 64,
    /* .MaxVertexTextureImageUnits = */ 32,
    /* .MaxCombinedTextureImageUnits = */ 80,
    /* .MaxTextureImageUnits = */ 32,
    /* .MaxFragmentUniformComponents = */ 4096,
    /* .MaxDrawBuffers = */ 32,
    /* .MaxVertexUniformVectors = */ 128,
    /* .MaxVaryingVectors = */ 8,
    /* .MaxFragmentUniformVectors = */ 16,
    /* .MaxVertexOutputVectors = */ 16,
    /* .MaxFragmentInputVectors = */ 15,
    /* .MinProgramTexelOffset = */ -8,
    /* .MaxProgramTexelOffset = */ 7,
    /* .MaxClipDistances = */ 8,
    /* .MaxComputeWorkGroupCountX = */ 65535,
    /* .MaxComputeWorkGroupCountY = */ 65535,
    /* .MaxComputeWorkGroupCountZ = */ 65535,
    /* .MaxComputeWorkGroupSizeX = */ 1024,
    /* .MaxComputeWorkGroupSizeY = */ 1024,
    /* .MaxComputeWorkGroupSizeZ = */ 64,
    /* .MaxComputeUniformComponents = */ 1024,
    /* .MaxComputeTextureImageUnits = */ 16,
    /* .MaxComputeImageUniforms = */ 8,
    /* .MaxComputeAtomicCounters = */ 8,
    /* .MaxComputeAtomicCounterBuffers = */ 1,
    /* .MaxVaryingComponents = */ 60,
    /* .MaxVertexOutputComponents = */ 64,
    /* .MaxGeometryInputComponents = */ 64,
    /* .MaxGeometryOutputComponents = */ 128,
    /* .MaxFragmentInputComponents = */ 128,
    /* .MaxImageUnits = */ 8,
    /* .MaxCombinedImageUnitsAndFragmentOutputs = */ 8,
    /* .MaxCombinedShaderOutputResources = */ 8,
    /* .MaxImageSamples = */ 0,
    /* .MaxVertexImageUniforms = */ 0,
    /* .MaxTessControlImageUniforms = */ 0,
    /* .MaxTessEvaluationImageUniforms = */ 0,
    /* .MaxGeometryImageUniforms = */ 0,
    /* .MaxFragmentImageUniforms = */ 8,
    /* .MaxCombinedImageUniforms = */ 8,
    /* .MaxGeometryTextureImageUnits = */ 16,
    /* .MaxGeometryOutputVertices = */ 256,
    /* .MaxGeometryTotalOutputComponents = */ 1024,
    /* .MaxGeometryUniformComponents = */ 1024,
    /* .MaxGeometryVaryingComponents = */ 64,
    /* .MaxTessControlInputComponents = */ 128,
    /* .MaxTessControlOutputComponents = */ 128,
    /* .MaxTessControlTextureImageUnits = */ 16,
    /* .MaxTessControlUniformComponents = */ 1024,
    /* .MaxTessControlTotalOutputComponents = */ 4096,
    /* .MaxTessEvaluationInputComponents = */ 128,
    /* .MaxTessEvaluationOutputComponents = */ 128,
    /* .MaxTessEvaluationTextureImageUnits = */ 16,
    /* .MaxTessEvaluationUniformComponents = */ 1024,
    /* .MaxTessPatchComponents = */ 120,
    /* .MaxPatchVertices = */ 32,
    /* .MaxTessGenLevel = */ 64,
    /* .MaxViewports = */ 16,
    /* .MaxVertexAtomicCounters = */ 0,
    /* .MaxTessControlAtomicCounters = */ 0,
    /* .MaxTessEvaluationAtomicCounters = */ 0,
    /* .MaxGeometryAtomicCounters = */ 0,
    /* .MaxFragmentAtomicCounters = */ 8,
    /* .MaxCombinedAtomicCounters = */ 8,
    /* .MaxAtomicCounterBindings = */ 1,
    /* .MaxVertexAtomicCounterBuffers = */ 0,
    /* .MaxTessControlAtomicCounterBuffers = */ 0,
    /* .MaxTessEvaluationAtomicCounterBuffers = */ 0,
    /* .MaxGeometryAtomicCounterBuffers = */ 0,
    /* .MaxFragmentAtomicCounterBuffers = */ 1,
    /* .MaxCombinedAtomicCounterBuffers = */ 1,
    /* .MaxAtomicCounterBufferSize = */ 16384,
    /* .MaxTransformFeedbackBuffers = */ 4,
    /* .MaxTransformFeedbackInterleavedComponents = */ 64,
    /* .MaxCullDistances = */ 8,
    /* .MaxCombinedClipAndCullDistances = */ 8,
    /* .MaxSamples = */ 4,
    /* .maxMeshOutputVerticesNV = */ 256,
    /* .maxMeshOutputPrimitivesNV = */ 512,
    /* .maxMeshWorkGroupSizeX_NV = */ 32,
    /* .maxMeshWorkGroupSizeY_NV = */ 1,
    /* .maxMeshWorkGroupSizeZ_NV = */ 1,
    /* .maxTaskWorkGroupSizeX_NV = */ 32,
    /* .maxTaskWorkGroupSizeY_NV = */ 1,
    /* .maxTaskWorkGroupSizeZ_NV = */ 1,
    /* .maxMeshViewCountNV = */ 4,
    /* .maxDualSourceDrawBuffersEXT = */ 1,

    /* .limits = */ {
        /* .nonInductiveForLoops = */ 1,
        /* .whileLoops = */ 1,
        /* .doWhileLoops = */ 1,
        /* .generalUniformIndexing = */ 1,
        /* .generalAttributeMatrixVectorIndexing = */ 1,
        /* .generalVaryingIndexing = */ 1,
        /* .generalSamplerIndexing = */ 1,
        /* .generalVariableIndexing = */ 1,
        /* .generalConstantMatrixVectorIndexing = */ 1,
    }
};

bool quit = false;

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        quit = true;
        return 0;

    case WM_PAINT:
        /*{
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);



            FillRect(hdc, &ps.rcPaint, (HBRUSH) (COLOR_WINDOW+1));

            EndPaint(hwnd, &ps);
        }*/
        // Force redraw of the entire window.
        // Needed so the main loop with the GetMessage() will work.
        // Without it GetMessage() will block the execution,
        // since there is no messages in the message queue,
        // which will eventually cause the rendering to stop
        RedrawWindow(hwnd, NULL, NULL, 0);
        return 0;

    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

// Here are some great examples how to create a surface with native window:
// https://www.programcreek.com/cpp/?CodeExample=create+surface
/*
VkSurfaceKHR Graphics::VKDevice::CreatePlatformSurface(VkInstance vkInstance, Window* window)
	{
		VkSurfaceKHR surface;

#ifdef LUMOS_USE_GLFW_WINDOWS
		glfwCreateWindowSurface(vkInstance, static_cast<GLFWwindow*>(window->GetHandle()), nullptr, (VkSurfaceKHR*)&surface);
#else
		VkWin32SurfaceCreateInfoKHR surfaceInfo;
		memset(&surfaceInfo, 0, sizeof(VkWin32SurfaceCreateInfoKHR));

		surfaceInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		surfaceInfo.pNext = NULL;
		surfaceInfo.hwnd = static_cast<HWND>(window->GetHandle());
		surfaceInfo.hinstance = dynamic_cast<WindowsWindow*>(window)->GetHInstance();
		vkCreateWin32SurfaceKHR(m_VKContext->GetVKInstance(), &surfaceInfo, nullptr, &surface);
#endif

		return surface;
	} 
*/

// There are also many more vulkan related conan packages:
// https://conan.io/center/search/vulkan

class VulkanRenderer
{
public:
    class Error: public vk::Error
    {
    public:
        explicit Error(std::string_view message);

    private:
        virtual const char * what() const VULKAN_HPP_NOEXCEPT override;

        std::string_view _message;
    };
    VulkanRenderer(HWND window, HINSTANCE instance);
    ~VulkanRenderer();

    bool draw();

private:
    struct PhysicalDevice
    {
        vk::raii::PhysicalDevice _physicalDevice{ std::nullptr_t{} };

        vk::SurfaceCapabilitiesKHR _surfaceCapabilities;
        std::vector<vk::SurfaceFormatKHR> _surfaceFormats;
        std::vector<vk::PresentModeKHR> _surfacePresentModes;
        vk::Extent2D _surfaceExtent;
        vk::SurfaceFormatKHR _selectedSurfaceFormat;
        vk::PresentModeKHR _selectedSurfacePresentMode;

        uint32_t _graphicsQueueIndex = -1;
        uint32_t _presentQueueIndex = -1;
    };
    struct Buffer
    {
        vk::raii::Buffer _buffer =          { std::nullptr_t{} };
        vk::raii::DeviceMemory _memory =    { std::nullptr_t{} };
    };
    struct UniformBufferObject
    {
        glm::mat4 model;
        glm::mat4 view;
        glm::mat4 proj;
    };

    void createInstance();
    void createSurface(HWND window, HINSTANCE instance);
    void selectPhysicalDevice();
    void createDevice();
    void createSwapchain(HWND window);
    void createDescriptorSetLayout();
    void createPipeline();
    void createFramebuffers();
    void createCommandPool();
    void createVertexBuffer();
    void createIndexBuffer();
    void createUniformBuffers();
    void createDescriptorPool();
    void createDescriptorSets();
    void createCommandBuffers();
    void createSynchronizationObjects();

    std::pair<vk::raii::Buffer, vk::raii::DeviceMemory> createBuffer(vk::DeviceSize size,
        vk::BufferUsageFlags usage,
        vk::MemoryPropertyFlags requiredProperties);

    void copyBuffer(const vk::raii::Buffer& source,
        const vk::raii::Buffer& destination,
        vk::DeviceSize size);

    void updateUniformBuffer(uint32_t imageIndex);

    void recreateSwapchain();

 #ifdef _DEBUG
    const std::vector<const char*> _layerNames = {
        "VK_LAYER_KHRONOS_validation"
    };
#else
    const std::vector<const char*> layerNames;
#endif
    const std::vector<const char*> _extensionNames = {
        // "VK_KHR_surface",
        // "VK_KHR_win32_surface",
        VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
        VK_KHR_SURFACE_EXTENSION_NAME,
        //VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    };
    std::vector<const char*> _deviceExtensionNames = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    };
    std::array<glm::vec3, 8> _vertexData = {
        glm::vec3( -0.5f, -0.5f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(  0.5f, -0.5f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f),
        glm::vec3(  0.5f,  0.5f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f),
        glm::vec3( -0.5f,  0.5f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f),
    };
    std::array<uint16_t, 6> _indexData = {
        0, 1, 2, 2, 3, 0
    };
    vk::raii::Context _context;
    vk::raii::Instance _instance =                          { std::nullptr_t{} };
    vk::raii::SurfaceKHR _surface =                         { std::nullptr_t{} };
    PhysicalDevice _physicalDevice;
    vk::raii::Device _device =                              { std::nullptr_t{} };
    vk::raii::Queue _graphicsQueue =                        { std::nullptr_t{} };
    vk::raii::Queue _presentQueue =                         { std::nullptr_t{} };
    vk::raii::SwapchainKHR _swapchain =                     { std::nullptr_t{} };
    std::vector<VkImage> _swapchainImages;
    std::vector<vk::raii::ImageView> _swapchainImageViews;
    vk::raii::DescriptorSetLayout _descriptorSetLayout =    { std::nullptr_t{} };
    vk::raii::RenderPass _renderPass =                      { std::nullptr_t{} };
    vk::raii::PipelineLayout _pipelineLayout =              { std::nullptr_t{} };
    vk::raii::Pipeline _pipeline =                          { std::nullptr_t{} };
    std::vector<vk::raii::Framebuffer> _framebuffers;
    vk::raii::CommandPool _commandPool =                    { std::nullptr_t{} };
    Buffer _vertexBuffer;
    Buffer _indexBuffer;
    std::vector<Buffer> _uniformBuffers;
    vk::raii::DescriptorPool _descriptorPool =              { std::nullptr_t{} };
    std::vector<vk::raii::DescriptorSet> _descriptorSets;
    std::vector<vk::raii::CommandBuffer> _commandBuffers;

    std::vector<vk::raii::Semaphore> _imageAvailableSemaphores;
    std::vector<vk::raii::Semaphore> _renderFinishedSemaphores;
    std::vector<vk::raii::Fence> _inFlightFences;
    std::vector<VkFence> _inFlightImages;
    size_t _currentFrame = 0;

    HWND _window = {};
};

VulkanRenderer::Error::Error(std::string_view message):
    _message(message)
{
    // Nothing to do yet
}

const char* VulkanRenderer::Error::what() const noexcept
{
    return _message.data();
}

VulkanRenderer::VulkanRenderer(HWND window, HINSTANCE instance):
    _window(window)
{
    createInstance();
    createSurface(window, instance);
    selectPhysicalDevice();
    createDevice();
    createSwapchain(window);
    createDescriptorSetLayout();
    createPipeline();
    createFramebuffers();
    createCommandPool();
    createVertexBuffer();
    createIndexBuffer();
    createUniformBuffers();
    createDescriptorPool();
    createDescriptorSets();
    createCommandBuffers();
    createSynchronizationObjects();
}

VulkanRenderer::~VulkanRenderer()
{
    _device.waitIdle();
}

bool VulkanRenderer::draw()
{
    _device.waitForFences({ *_inFlightFences[_currentFrame] }, VK_TRUE, UINT64_MAX);

    // Fetch image index from the swapchain
    // The imageAvailableSemaphore will be signaled when the image is available
    auto [nextImageResult, imageIndex] = _swapchain.acquireNextImage(UINT64_MAX, *_imageAvailableSemaphores[_currentFrame]);

    if(vk::Result::eErrorOutOfDateKHR == nextImageResult)
    {
        recreateSwapchain();
        return true;
    }

    // TODO: Fix inFlightImages... They are never assigned
    if(_inFlightImages[imageIndex] != VK_NULL_HANDLE)
    {
        _device.waitForFences({ _inFlightImages[imageIndex] }, VK_TRUE, UINT64_MAX);
    }

    // Submit the command buffer for execution

    // Semaphores to wait for
    vk::Semaphore waitSemaphores[] = { *_imageAvailableSemaphores[_currentFrame] };
    // Semaphores to signal when the command buffer is executed
    vk::Semaphore signalSemaphores[] = { *_renderFinishedSemaphores[_currentFrame] };
    // Specify the stages where the semaphores need to be waited for
    vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };

    updateUniformBuffer(imageIndex);

    vk::SubmitInfo submitInfo{};
    //submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    // TODO: This line seems fishy
    submitInfo.pCommandBuffers = &*_commandBuffers[imageIndex];
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    _device.resetFences({ *_inFlightFences[_currentFrame] });

    _graphicsQueue.submit({ submitInfo }, *_inFlightFences[_currentFrame]);

    // Present the rendered image on the surface
    vk::SwapchainKHR swapChains[] = { *_swapchain };
    vk::PresentInfoKHR presentInfo{};
    //presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    // We  are waiting for the submitted command buffers to be finished
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = nullptr; // Optional

    try
    {
        const vk::Result result = _presentQueue.presentKHR(presentInfo);

        if(vk::Result::eErrorOutOfDateKHR == result || vk::Result::eSuboptimalKHR == result)
        {
            recreateSwapchain();
        }
    }
    catch(vk::OutOfDateKHRError&)
    {
        // When the window is closed, vkQueuePresentKHR inside presentQueue.presentKHR will return OUT_OF_DATE,
        // and then an exception is thrown.
        recreateSwapchain();
        // return false;
    }
    catch(vk::Error&)
    {
        return false;
    }

    _currentFrame = (_currentFrame + 1) % _swapchainImages.size();

    return true;
}

void VulkanRenderer::createInstance()
{
    // Check if requires extensions are available on the device
    std::vector<vk::ExtensionProperties> extensions = vk::enumerateInstanceExtensionProperties();
    bool extensionsAvailable = true;

    for(const auto& extensionName: _extensionNames)
    {
        auto predicate = [&extensionName](const vk::ExtensionProperties& extension)
        {
            return  0 == strcmp(extensionName, extension.extensionName.data());
        };
        extensionsAvailable = std::find_if(extensions.begin(), extensions.end(), predicate) != extensions.end();
        if(!extensionsAvailable)
        {
            break;
        }
    }
    if(!extensionsAvailable)
    {
        throw Error("Required extensions are not available on instance level!");
    }

    // Check if required layers are available
    std::vector<vk::LayerProperties> layers = vk::enumerateInstanceLayerProperties();
    bool layersAvailable = true;
    for(const auto& layerName: _layerNames)
    {
        auto predicate = [&layerName](const vk::LayerProperties& layer)
        {
            return 0 == strcmp(layerName, layer.layerName);
        };
        layersAvailable = std::find_if(layers.begin(), layers.end(), predicate) != layers.end();
        if(!layersAvailable)
        {
            break;
        }
    }
    if(!layersAvailable)
    {
        throw Error("Required layers are not available on instance level!");
    }

    // Create instance
    vk::ApplicationInfo appInfo{};
    //appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    vk::InstanceCreateInfo createInfo{};
    //createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledLayerCount = static_cast<uint32_t>(_layerNames.size());
    createInfo.ppEnabledLayerNames = _layerNames.data();
    createInfo.enabledExtensionCount = static_cast<uint32_t>(_extensionNames.size());
    createInfo.ppEnabledExtensionNames = _extensionNames.data();

    _instance = _context.createInstance(createInfo);
}

void VulkanRenderer::createSurface(HWND window, HINSTANCE instance)
{
    vk::Win32SurfaceCreateInfoKHR surfaceCreateInfo{};
    //surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.hinstance = instance;
    surfaceCreateInfo.hwnd = window;

    _surface = _instance.createWin32SurfaceKHR(surfaceCreateInfo);
}

void VulkanRenderer::selectPhysicalDevice()
{
    std::vector<vk::raii::PhysicalDevice> physicalDevices = _instance.enumeratePhysicalDevices();

    // TODO: Check the physical devices
    // The one we need should have graphics queue, presentation queue
    // and proper swapchain support
    // return indices.isComplete() && extensionsSupported && swapChainAdequate;
    std::cout << "Physical devices:" << std::endl;
    for(auto& device: physicalDevices)
    {
        vk::PhysicalDeviceProperties properties = device.getProperties();
        vk::PhysicalDeviceFeatures features = device.getFeatures();
        std::vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties();
        std::vector<vk::LayerProperties> availableLayers = device.enumerateDeviceLayerProperties();

        // Check if requires device extensions are available on the device
        bool extensionsAvailable = true;
        for(const auto& extensionName: _deviceExtensionNames)
        {
            auto predicate = [&extensionName](const vk::ExtensionProperties& extension)
            {
                return  0 == strcmp(extensionName, extension.extensionName.data());
            };
            extensionsAvailable = std::find_if(availableExtensions.begin(), availableExtensions.end(), predicate) != availableExtensions.end();
            if(!extensionsAvailable)
            {
                break;
            }
        }
        if(!extensionsAvailable)
        {
            // This device does not support all the required extensions
            continue;
        }

        // Check if required layers are available
        bool layersAvailable = true;
        for(const auto& layerName: _layerNames)
        {
            auto predicate = [&layerName](const vk::LayerProperties& layer)
            {
                return 0 == strcmp(layerName, layer.layerName);
            };
            layersAvailable = std::find_if(availableLayers.begin(), availableLayers.end(), predicate) != availableLayers.end();
            if(!layersAvailable)
            {
                break;
            }
        }
        if(!layersAvailable)
        {
            // The device does not have all the required layers
            continue;
        }

        // Check if device has graphics and presentation queues
        std::vector<vk::QueueFamilyProperties> queueFamilies = device.getQueueFamilyProperties();
        std::optional<uint32_t> graphicsQueueIndex;
        std::optional<uint32_t> presentQueueIndex;

        for(uint32_t index = 0; index < queueFamilies.size(); ++index)
        {
            if(queueFamilies[index].queueFlags & vk::QueueFlagBits::eGraphics)
            {
                graphicsQueueIndex = index;
            }

            vk::Bool32 presentSupport = device.getSurfaceSupportKHR(index, *_surface);

            if(presentSupport)
            {
                presentQueueIndex = index;
            }
            if(graphicsQueueIndex && presentQueueIndex)
            {
                break;
            }
        }
        // The required queues are not present on the device
        if(!graphicsQueueIndex || !presentQueueIndex)
        {
            continue;
        }

        // Check if we have proper surface capabilities
        vk::SurfaceCapabilitiesKHR surfaceCapabilities = device.getSurfaceCapabilitiesKHR(*_surface);
        std::vector<vk::SurfaceFormatKHR> surfaceFormats = device.getSurfaceFormatsKHR(*_surface);
        std::vector<vk::PresentModeKHR> surfacePresentModes = device.getSurfacePresentModesKHR(*_surface);

        if(surfaceFormats.empty() || surfacePresentModes.empty())
        {
            continue;
        }

        std::cout << "\tChosen device: " << properties.deviceName << std::endl;

        _physicalDevice._physicalDevice = std::move(device);
        _physicalDevice._surfaceCapabilities = surfaceCapabilities;
        _physicalDevice._surfaceFormats = std::move(surfaceFormats);
        _physicalDevice._surfacePresentModes = std::move(surfacePresentModes);
        _physicalDevice._graphicsQueueIndex = graphicsQueueIndex.value();
        _physicalDevice._presentQueueIndex = presentQueueIndex.value();
    }
    if(!*_physicalDevice._physicalDevice)
    //if(physicalDevices.empty())
    {
        throw Error("Failed to find any physical device!");
    }
}

void VulkanRenderer::createDevice()
{
    std::set<uint32_t> queueIndices = {
        _physicalDevice._graphicsQueueIndex,
        _physicalDevice._presentQueueIndex,
    };
    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
    float queuePriority = 1.0f;

    for(uint32_t queueIndex: queueIndices)
    {
        vk::DeviceQueueCreateInfo queueCreateInfo{};
        //queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueIndex;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;

        queueCreateInfos.push_back(queueCreateInfo);
    }

    vk::PhysicalDeviceFeatures deviceFeatures{};
    vk::DeviceCreateInfo deviceCreateInfo{};
    //sdeviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
    deviceCreateInfo.pEnabledFeatures = &deviceFeatures;
    deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(_deviceExtensionNames.size());
    deviceCreateInfo.ppEnabledExtensionNames = _deviceExtensionNames.data();
    deviceCreateInfo.enabledLayerCount = static_cast<uint32_t>(_layerNames.size());
    deviceCreateInfo.ppEnabledLayerNames = _layerNames.data();

    _device = _physicalDevice._physicalDevice.createDevice(deviceCreateInfo);
    _graphicsQueue = _device.getQueue(_physicalDevice._graphicsQueueIndex, 0);
    _presentQueue = _device.getQueue(_physicalDevice._presentQueueIndex, 0);
}

void VulkanRenderer::createSwapchain(HWND window)
{
    _physicalDevice._surfaceCapabilities = _physicalDevice._physicalDevice.getSurfaceCapabilitiesKHR(*_surface);
    _physicalDevice._surfaceFormats = _physicalDevice._physicalDevice.getSurfaceFormatsKHR(*_surface);
    _physicalDevice._surfacePresentModes = _physicalDevice._physicalDevice.getSurfacePresentModesKHR(*_surface);
    // Select surface extent
    // UINT_MAX means, the surface can have any extent between min and max,
    // so if it is NOT that, we need to have that extent
    if(std::numeric_limits<uint32_t>::max() != _physicalDevice._surfaceCapabilities.currentExtent.width)
    {
        _physicalDevice._surfaceExtent = _physicalDevice._surfaceCapabilities.currentExtent;
    }
    else
    {
        RECT rect;
        GetClientRect(window, &rect);

        _physicalDevice._surfaceExtent.width = rect.right;
        _physicalDevice._surfaceExtent.height = rect.bottom;
        _physicalDevice._surfaceExtent.width = std::clamp(_physicalDevice._surfaceExtent.width,
            _physicalDevice._surfaceCapabilities.minImageExtent.width,
            _physicalDevice._surfaceCapabilities.maxImageExtent.width);
        _physicalDevice._surfaceExtent.height = std::clamp(_physicalDevice._surfaceExtent.height,
            _physicalDevice._surfaceCapabilities.minImageExtent.height,
            _physicalDevice._surfaceCapabilities.maxImageExtent.height);
    }

    // Select image count
    uint32_t imageCount = _physicalDevice._surfaceCapabilities.minImageCount + 1;
    // Clamp imageCount at the max image count of the surface.
    // Zero means there is no limit in the image count
    if(_physicalDevice._surfaceCapabilities.maxImageCount != 0 &&
       imageCount > _physicalDevice._surfaceCapabilities.maxImageCount)
    {
        imageCount = _physicalDevice._surfaceCapabilities.maxImageCount;
    }

    // Select surface format
    for(const auto& surfaceFormat: _physicalDevice._surfaceFormats)
    {
        if(surfaceFormat.format == vk::Format::eB8G8R8A8Srgb && surfaceFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
        {
            _physicalDevice._selectedSurfaceFormat = surfaceFormat;
            break;
        }
    }

    // Select presentation mode
    for(const auto& presentMode: _physicalDevice._surfacePresentModes)
    {
        if(vk::PresentModeKHR::eMailbox == presentMode)
        {
            _physicalDevice._selectedSurfacePresentMode = presentMode;
            break;
        }
    }

    std::set<uint32_t> queueIndices = {
        _physicalDevice._graphicsQueueIndex,
        _physicalDevice._presentQueueIndex,
    };
    std::vector<uint32_t> queueIndicesVector(queueIndices.begin(), queueIndices.end());
    vk::SwapchainCreateInfoKHR swapchainCreateInfo{};
    //swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCreateInfo.surface = *_surface;
    swapchainCreateInfo.minImageCount = imageCount;
    swapchainCreateInfo.imageFormat = _physicalDevice._selectedSurfaceFormat.format;
    swapchainCreateInfo.imageColorSpace = _physicalDevice._selectedSurfaceFormat.colorSpace;
    swapchainCreateInfo.imageExtent = _physicalDevice._surfaceExtent;
    swapchainCreateInfo.imageArrayLayers = 1;
    swapchainCreateInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;
    // If the needed queues are in the same queue family, we can use exclusive sharing mode
    // otherwise we use concurrent
    swapchainCreateInfo.imageSharingMode = size_t(1) == queueIndices.size() ? vk::SharingMode::eExclusive : vk::SharingMode::eConcurrent;
    swapchainCreateInfo.queueFamilyIndexCount = static_cast<uint32_t>(queueIndicesVector.size());
    swapchainCreateInfo.pQueueFamilyIndices = queueIndicesVector.data();
    swapchainCreateInfo.preTransform = _physicalDevice._surfaceCapabilities.currentTransform;
    // Always show the image as opaque
    swapchainCreateInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    swapchainCreateInfo.presentMode = _physicalDevice._selectedSurfacePresentMode;
    // Clip the obscured pixels
    swapchainCreateInfo.clipped = VK_TRUE;
    // No old swapchain. Needed if we are recreating the swapchain when e.g. the window is resized.
    swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

    _swapchain = _device.createSwapchainKHR(swapchainCreateInfo);

    // Get the images from the swapchain
    _swapchainImages = _swapchain.getImages();

    // Create image views for the images
    for(const auto& swapchainImage: _swapchainImages)
    {
        vk::ImageViewCreateInfo imageViewCreateInfo{};
        //imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = swapchainImage;
        imageViewCreateInfo.viewType = vk::ImageViewType::e2D;
        imageViewCreateInfo.format = _physicalDevice._selectedSurfaceFormat.format;
        // Do not mess with the components
        imageViewCreateInfo.components.r = vk::ComponentSwizzle::eIdentity;
        imageViewCreateInfo.components.g = vk::ComponentSwizzle::eIdentity;
        imageViewCreateInfo.components.b = vk::ComponentSwizzle::eIdentity;
        imageViewCreateInfo.components.a = vk::ComponentSwizzle::eIdentity;
        // The image is used as a colour target
        imageViewCreateInfo.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
        // No mipmap levels (well, only one)
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        // No arrays (well, only one)
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        _swapchainImageViews.push_back(_device.createImageView(imageViewCreateInfo));
    }
}

void VulkanRenderer::createDescriptorSetLayout()
{
    vk::DescriptorSetLayoutBinding binding;
    binding.binding = 0;
    binding.descriptorType = vk::DescriptorType::eUniformBuffer;
    // This can be used if the shader expects an array of uniforms
    binding.descriptorCount = 1;
    binding.stageFlags = vk::ShaderStageFlagBits::eVertex;
    binding.pImmutableSamplers = nullptr;

    vk::DescriptorSetLayoutCreateInfo createInfo;
    createInfo.bindingCount = 1;
    createInfo.pBindings = &binding;
    // createInfo.setBindings({ binding });

    _descriptorSetLayout = _device.createDescriptorSetLayout(createInfo);
}

void VulkanRenderer::createPipeline()
{
    //
    // Compile shaders
    //

    // Parse vertex shader code
    glslang::TShader vertexShader(EShLangVertex);
    const char* vertexShaderCode = R"(
#version 450

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 fragColor;

void main() {
    gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0);
    fragColor = inColor;
})";
    int vertexShaderCodeSize = static_cast<int>(strlen(vertexShaderCode));

    // See example how to compile here: https://github.com/KhronosGroup/glslang/blob/master/gtests/TestFixture.h
    vertexShader.setStringsWithLengths(&vertexShaderCode, &vertexShaderCodeSize, 1);
    vertexShader.setEnvInput(glslang::EShSourceGlsl, EShLangVertex, glslang::EShClientVulkan, 100);
    vertexShader.setEnvClient(glslang::EShClientVulkan, glslang::EShTargetVulkan_1_2);
    vertexShader.setEnvTarget(glslang::EShTargetSpv, glslang::EShTargetSpv_1_0);
    
    if(!vertexShader.parse(&DefaultTBuiltInResource, 100, false, EShMsgDefault))
    {
        const std::string message = "Failed to parse vertex shader: " + std::string(vertexShader.getInfoLog());

        throw Error(message);
    }

    // Link vertex shader
    glslang::TProgram vertexProgram;

    vertexProgram.addShader(&vertexShader);

    if(!vertexProgram.link(EShMsgDefault))
    {
        const std::string message = "Failed to link vertex shader: " + std::string(vertexProgram.getInfoLog());

        throw Error(message);
    }

    // Get vertex SPIR-V code
    glslang::TIntermediate* vertexIntermediate = vertexProgram.getIntermediate(EShLangVertex);

    if(nullptr == vertexIntermediate)
    {
        throw Error("No intermediate code for vertex program!");
    }

    std::vector<unsigned int> vertexSpvCode;
    glslang::SpvOptions spvOptions;
    spvOptions.generateDebugInfo = false;
    spvOptions.stripDebugInfo = true;
    spvOptions.disableOptimizer = false;
    spvOptions.optimizeSize = true;
    spvOptions.disassemble = false;
    spvOptions.validate = true;

    glslang::GlslangToSpv(*vertexIntermediate, vertexSpvCode, &spvOptions);

    // Parse fragment shader code
    glslang::TShader fragmentShader(EShLangFragment);
    const char* fragmentShaderCode = R"(#version 450

layout(location = 0) in vec3 fragColor;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = vec4(fragColor, 1.0);
})";
    int fragmentShaderCodeSize = static_cast<int>(strlen(fragmentShaderCode));

    fragmentShader.setStringsWithLengths(&fragmentShaderCode, &fragmentShaderCodeSize, 1);
    fragmentShader.setEnvInput(glslang::EShSourceGlsl, EShLangFragment, glslang::EShClientVulkan, 100);
    fragmentShader.setEnvClient(glslang::EShClientVulkan, glslang::EShTargetVulkan_1_2);
    fragmentShader.setEnvTarget(glslang::EShTargetSpv, glslang::EShTargetSpv_1_0);

    if(!fragmentShader.parse(&DefaultTBuiltInResource, 100, false, EShMsgDefault))
    {
        const std::string message = "Failed to parse fragment shader: " + std::string(fragmentShader.getInfoLog());

        throw Error(message);
    }

    // Link fragmens shader program
    glslang::TProgram fragmentProgram;

    fragmentProgram.addShader(&fragmentShader);

    if(!fragmentProgram.link(EShMsgDefault))
    {
        const std::string message = "Failed to link fragment program: " + std::string(fragmentProgram.getInfoLog());

        throw Error(message);
    }

    // Get fragment SPIR-V code
    glslang::TIntermediate* fragmentIntermediate = fragmentProgram.getIntermediate(EShLangFragment);

    if(nullptr == fragmentIntermediate)
    {
        throw Error("No intermediate code for fragment program!");
    }

    std::vector<unsigned int> fragmentSpvCode;

    glslang::GlslangToSpv(*fragmentIntermediate, fragmentSpvCode, &spvOptions);

    //
    // Create vulkan shaders
    //

    // Create vertex shader module
    vk::ShaderModuleCreateInfo vertexShaderModuleCreateInfo{};
    //vertexShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    vertexShaderModuleCreateInfo.codeSize = vertexSpvCode.size() * sizeof(unsigned int);
    vertexShaderModuleCreateInfo.pCode = vertexSpvCode.data();

    vk::raii::ShaderModule vertexShaderModule = _device.createShaderModule(vertexShaderModuleCreateInfo);

    // Create fragment shader module
    vk::ShaderModuleCreateInfo fragmentShaderModuleCreateInfo{};
    //fragmentShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    fragmentShaderModuleCreateInfo.codeSize = fragmentSpvCode.size() * sizeof(unsigned int);
    fragmentShaderModuleCreateInfo.pCode = fragmentSpvCode.data();

    vk::raii::ShaderModule fragmentShaderModule = _device.createShaderModule(fragmentShaderModuleCreateInfo);

    // Create shaders
    vk::PipelineShaderStageCreateInfo vertexShaderStageCreateInfo{};
    // vertexShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertexShaderStageCreateInfo.stage = vk::ShaderStageFlagBits::eVertex;
    vertexShaderStageCreateInfo.module = *vertexShaderModule;
    vertexShaderStageCreateInfo.pName = "main";

    vk::PipelineShaderStageCreateInfo fragmentShaderStageCreateInfo{};
    // fragmentShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragmentShaderStageCreateInfo.stage = vk::ShaderStageFlagBits::eFragment;
    fragmentShaderStageCreateInfo.module = *fragmentShaderModule;
    fragmentShaderStageCreateInfo.pName = "main";

    vk::PipelineShaderStageCreateInfo shaderStageCreateInfos[] = {
        vertexShaderStageCreateInfo,
        fragmentShaderStageCreateInfo
    };

    //
    // Configure fixed functions in the pipeline
    //

    // VertexInput
    // Describes what data will be passed to the vertex shader
    vk::VertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(float) * 6; // 3 for the position and 3 for the colour
    bindingDescription.inputRate = vk::VertexInputRate::eVertex;

    vk::VertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.location = 0;
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.format = vk::Format::eR32G32B32Sfloat;
    positionAttributeDescription.offset = 0;
    vk::VertexInputAttributeDescription colourAttributeDescription{};
    colourAttributeDescription.location = 1;
    colourAttributeDescription.binding = 0;
    colourAttributeDescription.format = vk::Format::eR32G32B32Sfloat;
    colourAttributeDescription.offset = sizeof(float) * 3;
    // TODO: This is very ineffective
    std::array attributeDescriptions = {
            positionAttributeDescription,
            colourAttributeDescription,
    };

    vk::PipelineVertexInputStateCreateInfo vertexInputStateCreateInfo{};
    //vertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
    vertexInputStateCreateInfo.pVertexBindingDescriptions = &bindingDescription; // Optional
    vertexInputStateCreateInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
    vertexInputStateCreateInfo.pVertexAttributeDescriptions = attributeDescriptions.data(); // Optional

    // InputAssemply
    // Describes what primites will be created from the vertices
    vk::PipelineInputAssemblyStateCreateInfo inputAssembly{};
    //inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = vk::PrimitiveTopology::eTriangleList;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    // Viewport state
    // Describes the viewport and scissor for the pipeline
    vk::Viewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(_physicalDevice._surfaceExtent.width);
    viewport.height = static_cast<float>(_physicalDevice._surfaceExtent.height);
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    vk::Rect2D scissor{};
    scissor.offset = vk::Offset2D(0, 0);
    scissor.extent = _physicalDevice._surfaceExtent;

    vk::PipelineViewportStateCreateInfo viewportState{};
    //viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    // Rasterization state
    // Describes how the primitives will be turned into fragments
    // Like the polygon mode, line width and cull mode
    vk::PipelineRasterizationStateCreateInfo rasterizer{};
    //rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = vk::PolygonMode::eFill;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = vk::CullModeFlagBits::eBack;
    rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    // Multisample state
    // Describes how multisampling should work for anti-aliasin
    // Enabling this requires GPU features to be enabled
    vk::PipelineMultisampleStateCreateInfo multisampling{};
    //multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = nullptr; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    // Depth stencil state
    // Describes how depth and stencil tests should work
    VkPipelineDepthStencilStateCreateInfo depthStencilStateCreateinfo{};

    // Color blend state
    // Describes how new colors from the fragment shaders should be blended
    // with the existing colors in the framebuffers.
    // You can specify blending operations on every framebuffer, for controlling
    // how the colors will be mixed by some mathematical operation
    // Or you can globally set a logical operation to be applied on every framebuffer.
    // If you set the second the first one will be disabled for all the framebuffers
    vk::PipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR |
                                          vk::ColorComponentFlagBits::eG |
                                          vk::ColorComponentFlagBits::eB |
                                          vk::ColorComponentFlagBits::eA;
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = vk::BlendFactor::eOne; // Optional
    colorBlendAttachment.dstColorBlendFactor = vk::BlendFactor::eZero; // Optional
    colorBlendAttachment.colorBlendOp = vk::BlendOp::eAdd; // Optional
    colorBlendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOne; // Optional
    colorBlendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eZero; // Optional
    colorBlendAttachment.alphaBlendOp = vk::BlendOp::eAdd; // Optional

    vk::PipelineColorBlendStateCreateInfo colorBlending{};
    //colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    // Here you can enable blending with a logical operator instead of some mathematical calculations
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = vk::LogicOp::eCopy; // Optional
    // For every framebuffer attached, you need to specify a VkColorBlendAttachmentState struct
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f; // Optional
    colorBlending.blendConstants[1] = 0.0f; // Optional
    colorBlending.blendConstants[2] = 0.0f; // Optional
    colorBlending.blendConstants[3] = 0.0f; // Optional

    // Dynamic state
    // The pipeline is more or less unchangeable, but certain parts can be dynamic,
    // but we need to tell it at creation time.
    // Then the values of these will be ignored at creation time,
    // and they have to be specified at drawing time
    VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_LINE_WIDTH
    };

    VkPipelineDynamicStateCreateInfo dynamicState{};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = 2;
    dynamicState.pDynamicStates = dynamicStates;

    // Pipeline layout
    // Defines the sets and push constants passed to the shaders later at drawing time
    vk::PipelineLayoutCreateInfo pipelineLayoutInfo{};
    //pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1; // Optional
    pipelineLayoutInfo.pSetLayouts = &*_descriptorSetLayout; // Optional
    pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

    _pipelineLayout = _device.createPipelineLayout(pipelineLayoutInfo);

    //
    // Create render pass
    //

    // Attachment description
    // Describes an attached image of the framebuffer
    vk::AttachmentDescription colorAttachment{};
    colorAttachment.format = _physicalDevice._selectedSurfaceFormat.format;
    // Sample count for multisampling
    colorAttachment.samples = vk::SampleCountFlagBits::e1;
    // Load and store operations for color and depth
    colorAttachment.loadOp = vk::AttachmentLoadOp::eClear;
    colorAttachment.storeOp = vk::AttachmentStoreOp::eStore;
    // Load and store operations for sencil
    colorAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    colorAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    // Initial layout is UNDEFINED since we do not really care
    colorAttachment.initialLayout = vk::ImageLayout::eUndefined;
    // This color attachment will be presented on the surface,
    // so it needs to be in the proper layout at the end
    colorAttachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

    // Create subpass(es)
    // A render pass can have multiple subpasses, like for postprocessing
    // Subpasses will reference attachments from the main render pass
    // Using subpasses cna result in better performance
    vk::AttachmentReference colorAttachmentRef{};
    // This is the index of the attachment to be used from the main render pass' list of attachments
    colorAttachmentRef.attachment = 0;
    // This is the input layout the render pass requires the attachment when it starts
    colorAttachmentRef.layout = vk::ImageLayout::eColorAttachmentOptimal;

    vk::SubpassDescription subpass{};
    // We need to specify this is a graphics subpass (can be e.g. computing or ray casting)
    subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    // Describe the dependencies between subpasses
    // There is only one subpass, but ther are two implicit passes at the beginning 
    // and end of the subpass.
    // Here we kind of describe that the commands recorded before vkCmdBeginRenderPass
    // needs to reach the VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT stage,
    // before the commands after vkCmdBeginRenderPass can enter VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT.
    // See the answer here: https://stackoverflow.com/questions/63320119/vksubpassdependency-specification-clarification
    // Sincs the externap subpass mainly there to set the attachement in the correct layout,
    // this dependency says, that the renderpass cannot write to the colour attachment,
    // until the external subpass is ready with setting up the layout for the attachment - I think
    vk::SubpassDependency subpassDependency{};
    subpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    subpassDependency.dstSubpass = 0;
    subpassDependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    //subpassDependency.srcAccessMask = 0;
    subpassDependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    subpassDependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;

    vk::RenderPassCreateInfo renderPassInfo{};
    //renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    // The list of the attachments
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    // The list of the subpasses
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &subpassDependency;

    _renderPass = _device.createRenderPass(renderPassInfo);

    //
    // Create the pipeline
    //
    vk::GraphicsPipelineCreateInfo pipelineInfo{};
    // pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStageCreateInfos;
    pipelineInfo.pVertexInputState = &vertexInputStateCreateInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = nullptr; // Optional
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr; // Optional
    pipelineInfo.layout = *_pipelineLayout;
    pipelineInfo.renderPass = *_renderPass;
    // Which subpass will be used in this pipeline
    pipelineInfo.subpass = 0;
    // Pipelines can "derive" from an existing if they share a lot of common functionality
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    // A pipeline can also derive from a to be created pipeline
    // This is the index into the VkGraphichPipelineCreateInfo array
    // passed later to the vkCreateGraphicsPipelines fucntions (notice it is in plural)
    pipelineInfo.basePipelineIndex = -1; // Optional

    _pipeline = _device.createGraphicsPipeline(nullptr, pipelineInfo);
}

void VulkanRenderer::createFramebuffers()
{
    for(const auto& imageView: _swapchainImageViews)
    {
        vk::ImageView attachments[] = {
            *imageView
        };

        vk::FramebufferCreateInfo framebufferCreateInfo{};
        //framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCreateInfo.renderPass = *_renderPass;
        framebufferCreateInfo.attachmentCount = 1;
        framebufferCreateInfo.pAttachments = attachments;
        framebufferCreateInfo.width =  _physicalDevice._surfaceExtent.width;
        framebufferCreateInfo.height = _physicalDevice._surfaceExtent.height;
        framebufferCreateInfo.layers = 1;

        _framebuffers.push_back(_device.createFramebuffer(framebufferCreateInfo));
    }
}

void VulkanRenderer::createCommandPool()
{
    vk::CommandPoolCreateInfo poolInfo{};
    //poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = _physicalDevice._graphicsQueueIndex;
    //poolInfo.flags = 0; // Optional

    _commandPool = _device.createCommandPool(poolInfo);
}

void VulkanRenderer::createVertexBuffer()
{
    const size_t size = _vertexData.size() * sizeof(float) * 3;
    auto [stagingBuffer, stagingBufferMemory] = createBuffer(size,
        vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    void* mappedMemory = stagingBufferMemory.mapMemory(0, size);
    memcpy_s(mappedMemory, size, _vertexData.data(), size);
    stagingBufferMemory.unmapMemory();

    std::tie(_vertexBuffer._buffer, _vertexBuffer._memory) = createBuffer(size,
        vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst,
        vk::MemoryPropertyFlagBits::eDeviceLocal);

    copyBuffer(stagingBuffer, _vertexBuffer._buffer, size);
}

void VulkanRenderer::createIndexBuffer()
{
    const size_t size = _indexData.size() * sizeof(uint16_t);
    auto [stagingBuffer, stagingBufferMemory] = createBuffer(size,
        vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);

    void* mappedMemory = stagingBufferMemory.mapMemory(0, size);
    memcpy_s(mappedMemory, size, _indexData.data(), size);
    stagingBufferMemory.unmapMemory();

    std::tie(_indexBuffer._buffer, _indexBuffer._memory) = createBuffer(size,
        vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst,
        vk::MemoryPropertyFlagBits::eDeviceLocal);

    copyBuffer(stagingBuffer, _indexBuffer._buffer, size);
}

void VulkanRenderer::createUniformBuffers()
{
    for(size_t index = 0; index < _swapchainImages.size(); ++index)
    {
        const size_t size = sizeof(UniformBufferObject);
        auto [buffer, memory] = createBuffer(size,
            vk::BufferUsageFlagBits::eUniformBuffer,
            vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);
        _uniformBuffers.emplace_back(Buffer());
        _uniformBuffers.back()._buffer = std::move(buffer);
        _uniformBuffers.back()._memory = std::move(memory);
        /*std::tie(_uniformBuffers[index]._buffer, _uniformBuffers[index]._memory) = createBuffer(size,
            vk::BufferUsageFlagBits::eUniformBuffer,
            vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);*/
    }
}

void VulkanRenderer::createDescriptorPool()
{
    vk::DescriptorPoolSize poolSize;
    poolSize.type = vk::DescriptorType::eUniformBuffer;
    poolSize.descriptorCount = static_cast<uint32_t>(_swapchainImages.size());

    vk::DescriptorPoolCreateInfo createInfo;
    // Either this or poolSize.descriptorCount does not need to be _swapchainImages.size()
    // I think maxSets is correct, but in each set we only need one descriptor
    createInfo.maxSets = static_cast<uint32_t>(_swapchainImages.size());
    createInfo.poolSizeCount = 1;
    createInfo.pPoolSizes = &poolSize;
    createInfo.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

    _descriptorPool = _device.createDescriptorPool(createInfo);
}

void VulkanRenderer::createDescriptorSets()
{
    std::vector<vk::DescriptorSetLayout> layouts(_swapchainImages.size(), *_descriptorSetLayout);
    vk::DescriptorSetAllocateInfo allocateInfo;
    allocateInfo.descriptorPool = *_descriptorPool;
    allocateInfo.descriptorSetCount = static_cast<uint32_t>(_swapchainImages.size());
    allocateInfo.pSetLayouts = layouts.data();

    _descriptorSets = _device.allocateDescriptorSets(allocateInfo);

    for(size_t index = 0; index < _descriptorSets.size(); ++index)
    // for(const auto& descriptorSet: _descriptorSets)
    {
        vk::DescriptorBufferInfo bufferInfo;
        bufferInfo.buffer = *_uniformBuffers[index]._buffer;
        bufferInfo.offset = 0;
        bufferInfo.range = sizeof(UniformBufferObject);
        vk::WriteDescriptorSet write;
        write.dstSet = *_descriptorSets[index];
        write.dstBinding = 0;
        write.dstArrayElement = 0;
        write.descriptorType = vk::DescriptorType::eUniformBuffer;
        write.descriptorCount = 1;
        write.pBufferInfo = &bufferInfo;
        write.pImageInfo = nullptr;
        write.pTexelBufferView = nullptr;

        _device.updateDescriptorSets({ write }, {});
    }
}

void VulkanRenderer::createCommandBuffers()
{
    // One command buffer for each framebuffer, since we need to link them
    VkCommandBufferAllocateInfo commandBufferAllocateInfo{};
    commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandBufferAllocateInfo.commandPool = *_commandPool;
    commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    commandBufferAllocateInfo.commandBufferCount = (uint32_t) _framebuffers.size();

    _commandBuffers = _device.allocateCommandBuffers(commandBufferAllocateInfo);

    //
    // Start command recording
    //

    for(size_t index = 0; index < _commandBuffers.size(); ++index)
    {
        // Begin command buffer recording
        vk::CommandBufferBeginInfo beginInfo{};
        beginInfo.pInheritanceInfo = nullptr; // Optional

        _commandBuffers[index].begin(beginInfo);

        // Begin render pass
        vk::ClearValue clearColor(vk::ClearColorValue(std::array<float, 4>{ 0.0f, 0.0f, 0.0f, 1.0f }));
        vk::RenderPassBeginInfo renderPassInfo{};
        renderPassInfo.renderPass = *_renderPass;
        renderPassInfo.framebuffer = *_framebuffers[index];
        renderPassInfo.renderArea.offset = vk::Offset2D(0, 0);
        renderPassInfo.renderArea.extent = _physicalDevice._surfaceExtent;
        renderPassInfo.clearValueCount = 1;
        renderPassInfo.pClearValues = &clearColor;

        _commandBuffers[index].beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
        // Bind pipeline
        _commandBuffers[index].bindPipeline(vk::PipelineBindPoint::eGraphics, *_pipeline);
        // Bind vertex buffer
        _commandBuffers[index].bindVertexBuffers(0, { *_vertexBuffer._buffer }, { 0 });
        // Bind index buffer
        _commandBuffers[index].bindIndexBuffer(*_indexBuffer._buffer, 0, vk::IndexType::eUint16);
        // Bind uniform buffer object
        _commandBuffers[index].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *_pipelineLayout, 0, { *_descriptorSets[index] }, { });
        // And finally draw
        _commandBuffers[index].drawIndexed(static_cast<uint32_t>(_indexData.size()), 1, 0, 0, 0);
        //_commandBuffers[index].draw(3, 1, 0, 0);
        // Finish render pass
        _commandBuffers[index].endRenderPass();
        _commandBuffers[index].end();
    }
}

void VulkanRenderer::createSynchronizationObjects()
{
     vk::SemaphoreCreateInfo semaphoreCreateInfo{};
    //semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    vk::FenceCreateInfo fenceCreateInfo{};
    //fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.flags = vk::FenceCreateFlagBits::eSignaled;

    // One semaphore for each image in the swapchain
    for(size_t index = 0; index < _swapchainImages.size(); ++index)
    {
        _imageAvailableSemaphores.push_back(_device.createSemaphore(semaphoreCreateInfo));
        _renderFinishedSemaphores.push_back(_device.createSemaphore(semaphoreCreateInfo));
        _inFlightFences.push_back(_device.createFence(fenceCreateInfo));
    }
    _inFlightImages.resize(_swapchainImages.size(), VK_NULL_HANDLE);
}

std::pair<vk::raii::Buffer, vk::raii::DeviceMemory> VulkanRenderer::createBuffer(vk::DeviceSize size,
    vk::BufferUsageFlags usage,
    vk::MemoryPropertyFlags requiredProperties)
{
    vk::BufferCreateInfo createInfo{};
    createInfo.size = size;
    createInfo.usage = usage;
    createInfo.sharingMode = vk::SharingMode::eExclusive; // TODO: Should this be a parameter too?

    vk::raii::Buffer buffer = _device.createBuffer(createInfo);

    vk::MemoryRequirements requirements = buffer.getMemoryRequirements();
    vk::PhysicalDeviceMemoryProperties memoryProperties = _physicalDevice._physicalDevice.getMemoryProperties();
    vk::MemoryPropertyFlags requiredMemoryProperties = requiredProperties;
    std::optional<uint32_t> memoryIndex;

    for(uint32_t index = 0; index < memoryProperties.memoryTypes.size(); ++index)
    //for(const auto& memoryProperty: memoryProperties.memoryTypes)
    {
        if((requirements.memoryTypeBits & (1 << index)) && (memoryProperties.memoryTypes[index].propertyFlags & requiredMemoryProperties) == requiredMemoryProperties )
        {
            memoryIndex = index;
            break;
        }
    }

    if(!memoryIndex)
    {
        throw Error("No suitable memory found for vertex buffer!");
    }

    vk::MemoryAllocateInfo allocateInfo{};
    allocateInfo.allocationSize = requirements.size;
    allocateInfo.memoryTypeIndex = memoryIndex.value();

    vk::raii::DeviceMemory bufferMemory = _device.allocateMemory(allocateInfo);

    buffer.bindMemory(*bufferMemory, 0);

    return { std::move(buffer), std::move(bufferMemory) };
}

void VulkanRenderer::copyBuffer(const vk::raii::Buffer& source,
    const vk::raii::Buffer& destination,
    vk::DeviceSize size)
{
    vk::CommandBufferAllocateInfo allocateInfo;
    allocateInfo.commandBufferCount = 1;
    allocateInfo.commandPool = *_commandPool;
    allocateInfo.level = vk::CommandBufferLevel::ePrimary;

    std::vector<vk::raii::CommandBuffer> commandBuffer = _device.allocateCommandBuffers(allocateInfo);

    vk::CommandBufferBeginInfo beginInfo;
    beginInfo.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;

    commandBuffer[0].begin(beginInfo);

    vk::BufferCopy copy;
    copy.srcOffset = 0;
    copy.dstOffset = 0;
    copy.size = size;

    commandBuffer[0].copyBuffer(*source, *destination, { copy });
    commandBuffer[0].end();

    vk::SubmitInfo submitInfo;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &*commandBuffer[0];

    _graphicsQueue.submit({ submitInfo });
    // Instead of waiting for the queue to be idle, we could use semaphores
    _graphicsQueue.waitIdle();
}

void VulkanRenderer::updateUniformBuffer(uint32_t imageIndex)
{
    static float rotation = 0.0f;
    Buffer& buffer = _uniformBuffers[imageIndex];
    UniformBufferObject ubo;
    const size_t size = sizeof(UniformBufferObject);
    const float aspectRatio = static_cast<float>(_physicalDevice._surfaceExtent.width) /
        static_cast<float>(_physicalDevice._surfaceExtent.height);

    rotation += 0.01f;
    ubo.model = glm::mat4(1.0f);
    ubo.model = glm::rotate(ubo.model, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.view = glm::lookAt(glm::vec3(2.0f), glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    ubo.proj = glm::perspective(glm::radians(45.0f), aspectRatio, 0.1f, 10.0f);
    ubo.proj[1][1] *= -1;

    void* mappedMemory = buffer._memory.mapMemory(0, size);
    memcpy_s(mappedMemory, size, &ubo, size);
    buffer._memory.unmapMemory();
}

void VulkanRenderer::recreateSwapchain()
{
    _device.waitIdle();
    _commandBuffers.clear();
    _framebuffers.clear();
    // TODO: Need to do it like this, since I think there is a bug in the vk-hpp
    //       A clear method is coming, see here: https://github.com/KhronosGroup/Vulkan-Hpp/issues/1202
    // TODO: This is causing errors
    vkDestroyPipeline(*_device, *_pipeline, nullptr);
    // This line does not destroy the pipeline although it should, since it is a raii
    _pipeline = { std::nullptr_t{} };
    vkDestroyPipelineLayout(*_device, *_pipelineLayout, nullptr);
    _pipelineLayout = { std::nullptr_t{} };
    vkDestroyRenderPass(*_device, *_renderPass, nullptr);
    _renderPass = { std::nullptr_t{} };
    _swapchainImageViews.clear();
    _swapchainImages.clear();
    vkDestroySwapchainKHR(*_device, *_swapchain, nullptr);
    _swapchain = { std::nullptr_t{} };
    _uniformBuffers.clear();
     vkDestroyDescriptorPool(*_device, *_descriptorPool, nullptr);
    _descriptorPool = { std::nullptr_t{} };
    // TODO: Clean and recreate fences and semaphores too
    createSwapchain(_window);
    createPipeline();
    createFramebuffers();
    createUniformBuffers();
    createDescriptorPool();
    createDescriptorSets();
    createCommandBuffers();
}

int main()
{
    glslang::InitializeProcess();
    //ShInitialize();

    //
    // Create native window
    //

    // Register the window class.
    const char CLASS_NAME[]  = "Sample Window Class";
    
    WNDCLASS wc = { };

    wc.lpfnWndProc   = WindowProc;
    wc.hInstance     = GetModuleHandle(nullptr);
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    // Create the window.
    // Looks like this window is not resizable, at least the surface says its surrent size,
    // max and min size are the same, later on the in the swapchain creation phase
    HWND hwnd = CreateWindowEx(
        0,                              // Optional window styles.
        CLASS_NAME,                     // Window class
        "Learn to Program Windows",    // Window text
        WS_OVERLAPPEDWINDOW,            // Window style

        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

        NULL,       // Parent window    
        NULL,       // Menu
        wc.hInstance,  // Instance handle
        NULL        // Additional application data
        );

    if (hwnd == NULL)
    {
        std::cerr << "Failed to create window!" << std::endl;
        return EXIT_FAILURE;
    }

    // Create renderer
    VulkanRenderer renderer(hwnd, GetModuleHandle(nullptr));
    // Show the window
    ShowWindow(hwnd, 1);

    try
    {
        

        // Run the message loop.
        MSG msg = { };
        while (GetMessage(&msg, NULL, 0, 0))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);

            // TODO: Ugly like hell. Quit needs to handled properly
            if(!quit)
            {
                if(!renderer.draw())
                {
                    break;
                }
            }
        }
    }
    catch(vk::Error&)
    {
        
    }

    //
    // Cleanup
    //

    glslang::FinalizeProcess();
    //ShFinalize();

    return EXIT_SUCCESS;
}
