# Use Vulkan on high level

## Create an instance

To start working with Vulkan, you first need to create an instance. To create an instance, you need to specify some generic information about the application, like its name, its version, and what version of the Vulkan it is going to use.

For the instance you also specify a list of enabled layers and extensions. Layers are useful for debugging purposes, so it is the best to enable them only on debug builds. Extensions are extending Vulkan. These are optional and you do not need to specify them if your application can work with core Vulkan functionality.

To see the available layers and extensions you can list them even before you create the instance, to make sure the needed layers and extensions are supported by the machine you application is running on. use `vkEnumerateInstanceExtensionProperties` to get the list of available extensions and `vkEnumerateInstanceLayerProperties` to get the available layers.

## Select a physical device
Using the created instance, you can list the available physical devices. Host machines can have multiple physical devices (e.g. integrated and dedicated video cards), so you can fetch different properties of the physical devices to pick the best one for you application at runtime. Use `vkGetPhysicalDeviceProperties` and `vkGetPhysicalDeviceFeatures` to list all the informations about the phyisical devices. You can also check the available extensions and layers on the phyisical devices. use `vkEnumerateDeviceExtensionProperties` and `vkEnumerateDeviceLayerProperties` to do that. You might also want to check what queue families the phyisical device support and its swapchain capabilities (see later) when picking up the right one.

## Create logical device
After the right physical device is selected, we can create a logical device. It is just an abstraction over the the phyisical one, with a specified set of features, queues and layers. You will mostly work with this logical device after the initialization.

## Surfaces and swapchains
Usually when creating an engine, we would like to show the rendered images. For that we need a Khronos extension called `VK_KHR_surface`. A surface is an area on the screen the images can be drawn to. Creating a surface is very platform specific, since it usually means we need some window to draw on. Because of that there are platform specific surface extensions as well. For Windows it is called `VK_KHR_win32_surface`. You do not need to remember this literal strings, the Vulkan API has macros for them. Use `VK_KHR_SURFACE_EXTENSION_NAME` and `VK_KHR_WIN32_SURFACE_EXTENSION_NAME` instead of literal strings.

A surface is not enough to be able to draw on a window and present the images on it. For that we need a swapchain, which is basically a list of images the application will render to. The swapchain will take care of presenting those images on the surface.

Creating the swapchain is highly dependant on the surface, so to create the proper swapchain with the proper images, we need to know the surface capabilities. To get that use `vkGetPhysicalDeviceSurfaceCapabilitiesKHR`. It will contain information like the minimum, maximum and current extent of the surface (in pixels), the minimum and maximum number of images it can handle, what pixel formats it can accept and what kind of presentation modes it supports. Based on these informations, we can set up the swapchain with the right number of images in the right format and extent.

Creating the swapchain will create the images for us, which can be fetched from the swapchain. From those images we can create image views, which specify how the textures will be treated in the application. E.g. if it is a texture for colour output, if it is a 2D texture, the number of mipmap levels and so on.

Swapchains are basically immuitable, so whenever the surface changes - e.g. the window is resized - the swapchain needs to be recreated from scratch.

## Creating a render pipeline

A render pipeline consists of several steps:

* Input assembler
  * Fixed
* Vertex shader
  * Programmable
* Tessalation
  * Programmable
* Geometry shader
  * Programmable
* Rasteriation
  * Fixed
* Fragment shader
  * Programmable
* Color blending
  * Fixed

### Using shaders for programmable pipeline steps
Vulkan requires the shaders to be in a binary format called SPIR-V. This is an intermediate binary format, so shaders can be written in any language which can be compiled to SPIR-V. This helps to simplify the drivers, sicne they do not need to contain a full high level compiler. To compile source codes (either GLSL or HLSL) to  SPIR-V, we cna use glslang from chronos. The compilation can happen offline, using the standalone tool called glslangValidator, or use the glslang library, and compile the code on the fly.

When the compilation is ready, and you have the binary code, then you can create shader modules to be used in the pipeline. Use `VkShaderModuleCreateInfo` with `vkCreateShaderModule` to create them. You only need to specify the binary code for a shader module.

The shader modules can be used to create pipeline shader stages. `VkPipelineShaderStageCreateInfo` structs can be used to specify which shader module will be used at which stage. You do not need to create the stages themselves. They will automatically be created when the pipeline itself is created.

### Set up the fixed pipeline steps
Here we have a ton of options to specify before we can create the pipeline itself. Here are the main ones:

* Vertex input state
  * Options to describe the vertex data sent to the vertex shader
  * Use `VkPipelineVertexInputStateCreateInfo` to specify your vertex data format
* Input assemply state
  * Options to specify what primitives to construct from the vertices
  * Use `VkPipelineInputAssemblyStateCreateInfo` to specify the primites
* Viewport state
  * Describes the viewport and the scissor for the pipeline
  * Use `VkPipelineViewportStateCreateInfo` which requires a `VkViewport` for the viewport settings and a `VkRect2D` for the scissor.
* Rasterization state
  * Options to specify how to turn the primitives into fragmens.
    * Polygon mode
    * Cull mode
    * Line width
    * etc.
  * Use `VkPipelineRasterizationStateCreateInfo` to specify all the options you need
* Multisample state
  * Options of multisampling for anti aliasing. Requires the right GPU features to be enabled
  * Use `VkPipelineMultisampleStateCreateInfo` to describe how multisampling should work
* Depth stencil state
  * Options for depth and stencil tests
  * Use `VkPipelineDepthStencilStateCreateInfo` tp describe how depth tests should work, and what stencil functions to use
* Color blend attachment state
  * Options how to blend colour outputs from the fragment shader with exisint colours in the attachments
    * There are two methods
      * You can descirbe a mathematical operation to blend colours for each attachment
      * Or you can specify a global logical operation to do the blending. If you have global blending attachment specific blendings will be turned off
  * Use as many `VkPipelineColorBlendAttachmentState` as you need to specify the color blending mode for each attachments you have
  * Use `VkPipelineColorBlendStateCreateInfo` to specify the global logical blending if you need that.
* Dynamic state
  * A pipeline is basically immutable, but certain properties can be changed at runtime. To allow it you need to specify them at creation time, and later on specify the values for every draw call. The values set at the creation stage will be ignored.
    * Examples
      * Viewport
      * Line width
  * Use a `VkPipelineDynamicStateCreateInfo` struct with a list of `VkDynamicState` to make them dynamic.
* Pipeline layout
  * Defines the sets and push constants used to pass data to the shaders (like uniforms in OpenGL).
    * A pipeline layout actually needs to be created not like the other states we had so far.
  * Use `VkPipelineLayoutCreateInfo` to desribe your sets and push constants and `vkCreatePipelineLayout` to actually create it.

### Create render pass
A render pass describes all the attachment used and how they will be handled. We can describe the colour and depth attachments e.g.. An attachment description (`VkAttachmentDescription`) describes an attachment, its format, the nubmer of samples, the load and store operations and its initial and final layout.

Load and store operations describe what to do with the attachment when the renderpass starts and finishes. E.g. you might not need a depth buffer after the render pass has finished, so you can specify that you "do not care", and save some bandwidth by not saving the result of the renderpass.

Initial and final layouts describe the wanted layouts of the attachment at the beginning and at the end of the render pass. A layout is different than a format. A format desribes ech pixels of the attachment, while the layout specifies how these pixels are placed in the memory. Certain operations requires different layouts for the best performance.

A render pass can contain several subpasses (but it needs to contain at least one). It can be helpful if you have a chain of renderpasses using each others outputs as input, like a chain of postprocess effects. Subpasses can reference attachments (`VkAttachmentReference`) from the main renderpass and the layout the renderpass requies that attachment to be in.

At the end a renderpass is just a collection of attachments and subpasses. use `VkRenderPassCreateInfo` and `vkCreateRenderPass` to create it.

### Create the pipeline
Finally it is time to create the pipeline itself. `VkGraphicsPipelineCreateInfo` is just a giant struct to hold references to all the states we have created. It will also have a handle to the render pass, but only one renderpass can be used with the pipeline itself. Pipelines can also "inherit" from another pipeline, if they share a lof of common properties. The "base pipeline" can either be specified with a `VkHandle` or an index. The latter is used as an index when multiple pipelines are created at once, since `vkCreateGraphicsPipelines` is a little bit special. As its name suggests more than one pipeline can be created with this call. If you create more than one, the index of the base pipeline will point to the "soon to be created" pipeline for this call.

### Create framebuffers
Framebuffers are containins all the actual attachments described in the render pass. Framebuffers need to be compatible with the renderpass they are used with. Compatible framebuffers need to have the same number of attachments as the renderpass they are used with. Attachments are bound by there indices. Framebuffers references ImageViews from the SwapChain created earlier. Since a swapchain can return different images for each frame we need to create a framebuffer for each image, since they are used as the colour attachment.

## Create commands
Commands are used to execute draw operations. Applications record their commands, and later they can submit and execute them on the device.

First we need to create a command pool which can be used to allocate command buffers. A command pool is tied to a queue family, like the graphich queue family.

After having the command pool, we can create command buffers. Command buffers need to be tied to a framebuffer, so it is the best to have as many commandbuffers as many framebuffers (so basically as many as many images we have in the swap chain)

### Command buffer recording
Recording commands to a buffer start with calling `vkBeginCommandBuffer` with the right arguments. Only this and its pair `vkEndCommandBuffer` returns a succes/failure indicator, other recording functions return void. If the begin succeeded the first thing to do is to bind the render pass and the frame buffer to the command buffer. This can be done with the `vkCmdBeginRenderPass` call. After that a pipeline needs to be bound by calling `vkCmdBindPipeline`. Now everything is glued together and we can record the actual draw commands with `vkCmdDraw`.

When we are done with the drawings, we need to finish the render pass by calling `vkCmdEndRenderPass` and finish the recording itself with `vkEndCommandBuffer`. Do not forget to check the return value!

Note that all the command recording functions start with `vkCmd` and they are all void.

## Rendering and presentation
To execute the recorded commands, we need an image for the colour attachment. That we can easily get from the swapchain. Then we need to submit the recorded commands and finally present the rendered image on the surface.

This sounds fairly simple, but since all of these operations are async, we need to do heavy syncronisation between the steps.

For syncronisation we can either use `VkSemaphore` or `VkFence`. Semaphores are syncronization objects which can only be waited and signalled on the GPU, so they are perfect to syncronize jobs on the device. Fences on the other hand can be waited on the CPU, so it can be used to sync the applications with the rendering.

All the commands used in this part of the rendering accepts syncronisation objects to make sure the different steps are executed in the right order.

To fetch an image from the swapchain we can use `vkAcquireNextImageKHR`. It is an extension - as usually everything related to surfaces an swapchains - and accepts a `VkSemaphore` which will be signalled when the selected image is ready to be used.

To submit the recorded commands to the graphics queue we can use `vkQueueSubmit`. This has a `VkSubmitInfo` parameter where we can specify semaphores to wait for before the commands are executed and semaphores to be signalled when the commands are executed. The `vkQueueSubmit` command also accepts a `VkFence`, which will also be signalled when the commands are executed.

Finally to present the rendered image we can use `vkQueuePresentKHR` with the present queue. In its `VkPresentInfoKHR` parameter we can specify semaphores to wait on before actually present the image.

With these syncronisation objects as parameters we can chain these steps, so they will be executed in the right order. When the image is ready from the swapchain the specified semaphore wil lbe signalled and the commands will start executing. When they are finished the specified semaphores will be signalled again, and so the presentation can finally start.

To make out the most of the swapchain we should not stop and wait after the first frame is rendered, but keep on rendering, so at any point in time there can actually be more frames waiting to be presented. To handle that it is a good practice to have as many semaphores and fences as many images in the swapchain, so when the new frame is actually presented ont he surface it is as up to date as possible.

### Swapchain recreation
It can happen that the surface is no longer valid  and the images used in the swapchain cannot be presented anymore. We can detect these changes by inspecting the return value of `vkAcquireNextImageKHR` and `vkQueuePresentKHR`. Their return value can tell us if the surface is out of date or not optimal for presentation anymore.

In such a case the swapchain needs to be recreated and all the Vullkan objects which are dependent on it, namely:

  * The whole pipeline
    * Since many parameters are (or can be) dependent on the surface, e.g. Viewport
    * That also means recreating the pipeline layout, the render pass and so on
  * The framebuffers
    * Since they are using the image views from the swapchain
  * The command buffers
    * Since they are bound to the renderpass in the pipeline
  * The uniform buffers
    * Since we need to have as many uniform buffer as many images in the swapchain
  * The descriptor sets and the descriptor pool
    * Since we need to have as many descriptors as many images in the swapchain
  * Fences and semaphores
    * Since we need to have as many of them as many images in the swapchain

## Using vertex and index buffers
### Creating buffers

Creating buffers is a two step process in vulkan, since we need to create a `VkBuffer` object and separately allocate the memory for that with a `VkDeviceMemory`. Then the buffer needs to be bound to the actual device memory.

Creating a buffer is simple, we jsut need to specify its size, usage and sharing mode in a `VkBufferCreateInfo`, and then call `vkCreateBuffer`.

Allocating the memory for it is a little bit more complex. First we need to fetch the buffer's memory requirements with `vkGetBufferMemoryRequirements`. This call will fill in a `VkMemoryRequirements` struct for us with all the information about the buffer's memory requirements, namely the real size needed on the device, the memory alignment and a `uint32_t` flag for the supported memory types.

Then we need to match these requirement with the memory types of the physical device, to decide where the allocation should happen. To do that we need to fetch the memory properties of the physical device by calling `vkGetPhysicalDeviceMemoryProperties`. This will fill in a `VkPhysicalDeviceMemoryProperties` struct with data about the memory and heap types on the device. We need to check the memory types and their properties.

The `uint32_t` flag from the buffer's memory requirements cna be used to determine which items from the phyisical device's memory types can be used. The required properties are application dependent. Properties can be e.g. if the memory is visible on the host or only on the device.

Since vertex and index data is not changed it is the best to make the memory for it device local, meaning it is only accessible by the VGA card. But then we have the problem to upload the data for it from the application. This can be solved by using a so called stagin buffer, which is a similar buffer but with diffent properties. We can set up the staging buffer to be host visible, so we can map its memory and copy the required data there. Then we can use transfer commands to copy the content of the staging buffer on the device to the final vertex or index buffer.

To do that we need a queue with transfer support, but by definition every graphics queue is a transfer queue at the same time, so we can use that one for this.

Executing transfer commands is pretty much the same as executing the graphics commands in the render loop. We use the `vkBeginCommandBuffer` and `vkEndCommandBuffer` buffers to start and stop the recording. `vkCmdCopyBuffer` is the actual command to copy data between buffers, and then we can submit the buffer by `vkQueueSubmit`.

To make sure the destination buffer is filled with the required data by the time they are needed, we can call `vkQueueWaitIdle` on the tranfer queue, or use some other synchronization methods, like `VkFence`s or `VkSemapore`s.

### Changing the pipeline
To be able to use vertex buffer the pipeline needs to be changed so it knows how to interpret the data in the buffers and pass them to the shaders. Use the `VkPipelineVertexInputStateCreateInfo` struct to describe the vertex bindings in the pipeline. This struct is basically a list of binding descriptors and vertex attribute descriptors.

The binding descriptors (`VkVertexInputBindingDescription`) describes a buffer binding. Where it is bound to, the stride size and the input rate of the data in the buffer. The input rate can be either vertex or instance.

The vertex attribute descriptors (`VkVertexInputAttributeDescription`) describes each data element in the buffer, by specifying its binding again, its location, the format of the data and the offset to it within the buffer's stride. The format is a little bit strange sometimes, since you need to use image format descriptors for every attribute. What matters is that the number of elements in the attribute and their type match. E.g. if the attribute is a 3 dimensional vector for the vertex position using floats, the format should be `VK_FORMAT_R32G32B32_SFLOAT`. You can see that the format is about RGB values, but what matters is that the elements are the same size (32 bit signed floats in this case)

### Binding the buffers
The final step is to actually use the buffers during draw. This is done by binding them on the command buffer by calling `vkCmdBindVertexBuffers` for vertex and `vkCmdBindIndexBuffer` for the index buffer. In case of index buffer do not forget to use indexed drawing by calling `vkCmdDrawIndexed`.

## Uniform buffers
### Descriptor layout
For uniform buffers we need to use descriptor sets. In Vulkan we cna have descriptor sets which can have different bindings where we can bind the descriptors and those descriptors can refer to buffers.
So first we need to create buffers like we discussed previously.
Then the next step to change the pipeline's layout, and specify the descriptor sets and in those descripor sets, the bindings.
In code we first specify a binding with `VkDescriptorSetLayoutBinding` where we can specify the index of the binding the the descriptor type (e.g. uniform buffer) and the descriptor count, if it is used as an array.
Also we need to specify which stages in the pipeline the binding will be used (e.g. the vertex shader stage)
Then we can create a `VkDescriptorSetLayout` by specifying all the bindings in it by an array of `VkDescriptorSetLayoutBinding`s.
Then it is time to specify the descriptor sets in the pipeline layout. There we can add all the descriptor sets again as an array of `VkDescriptorSetLayout`s.

### Desciptor set
Descriptor sets allocated from a descriptor pool, so first we need to create that pool.
A descriptor pool has a maximum size of the descriptor sets, and for each set a maximum number of descriptors.
With a `VkDescriptorPoolSize` we can specify the number and type of descriptors we want to allocate from a set.
Then in a `VkDescriptorPoolCreateInfo` we specify an array of `VkDescriptorPoolSize`s and the number of maximum sets. 
After the pool is created it can be used to allocate descriptor sets. Vulkan does now allow to bind descriptors alone, we can only bind descriptor sets, which can hold multiple descriptors.
To allocate descriptor sets we can use `VkDescriptorSetAllocateInfo` and fill in the information, like which pool to allocate from, for what decriptor set layout and how many sets we need.
When the allocation is done, we still need to "connect" the descriptors with the actual buffer. This can be done by calling `vkUpdateDescriptorSets` and either specifing write or copy. Using `VkWriteDescriptorSet` we can update a descriptor. In a `VkWriteDescriptorSet` struct we can specify which descriptor set at which binging (this is the actual descriptor) we want to update. We need to specify the descriptor type again, and the number of descriptors to update (needed for arrays). In a `VkDescriptorBufferInfo` we can describe which buffer (and which part of it) will be updated. Here is where descriptors are "connected" to buffers.

**TODO**: Not clear how writing really works. In the code we only invoke it once in the `createDescriptorSets`, but never in the render loop, but in the render loop we constantly update the uniform buffer. Is there some monitoring over the buffer's content, and the writing is always done whenever it is updated?

## Links
https://vkguide.dev/docs/chapter-4/descriptors/
